<?php

class Building{
	// private access modifier disables direct access to an object's property or methods
	// protected access modifier allows inheritance of properties and methods to child classes. However, it will still disable direct access to its properties and methods.
	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
}

class Condominium extends Building{
	// Write the getName and setName after demonstrating he output of private access modifier.
	// These functions serve as the intermediary in accessing the object's property.
	// These functions therefore defines whether an object's property can be accessed or changed.
	// These functions are called getters and setters and implement the encapsulation of an object's data.
	
	public function getName(){
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;

	}

	/*public function setFloors($floors){
	    $this->floors = $floors;
	    return $this;
	}*/

}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon city, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');
